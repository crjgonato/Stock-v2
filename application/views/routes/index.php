
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage
      <small>Routes</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
      <li class="active">Routes</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
     <!-- <h1>HELLO WORLD</h1> -->
     <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

        <?php if(in_array('createRoutes', $user_permission)): ?>
          <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#addModal">Add Route</button>
          <br /> <br />
        <?php endif; ?>

        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Manage Routes</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="manageTable" class="table ">
              <thead>
              <tr>
                <th>Routes</th>
                <th>Coverage</th>
                <th>Loads</th>
                <th>Units</th>
                <th>Returns</th>
                <th>Reloads</th>
                <th>Expenses</th>
                <th>Status</th>
                <?php if(in_array('updateRoutes', $user_permission) || in_array('deleteRoutes', $user_permission)): ?>
                  <th>Action</th>
                <?php endif; ?>
              </tr>
              </thead>

            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
   </section>
</div>

<?php if(in_array('createRoutes', $user_permission)): ?>
<!-- create route modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Route</h4>
      </div>

      <form role="form" action="<?php echo base_url('routes/create') ?>" method="post" id="createForm">

        <div class="modal-body">

          <div class="form-group">
            <label for="routes">Route</label>
            <input type="text" class="form-control input-sm" id="route_name" name="route_name" placeholder="Enter route" autocomplete="off">
          </div>
         <div class="form-group">
            <label for="coverage">Coverage</label>
            <input type="text" class="form-control input-sm" id="coverage" name="coverage" placeholder="Enter coverage" autocomplete="off">
          </div>
            <div class="form-group">
            <label for="loads">Loads</label>
            <input type="text" class="form-control input-sm" id="loads" name="loads" placeholder="Enter loads" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="units">Unit</label>
            <input type="text" class="form-control input-sm" id="units" name="units" placeholder="Enter unit" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="expenses">Expenses</label>
            <input type="text" class="form-control input-sm" id="expenses" name="expenses" placeholder="Enter unit" autocomplete="off">
          </div>
          <!-- <div class="form-group">
            <label for="brand_name">Date</label>
            <input type="text" class="form-control input-sm" id="date" name="date" placeholder="Enter route here" autocomplete="off">
          </div> -->

          <!-- <div class="form-group">
                <label for="date">Date:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="date" id="date" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                </div>
                
            </div> -->

          <div class="form-group">
            <label for="active">Status</label>
            <select class="form-control input-sm" id="active" name="active">
              <option value="1">Active</option>
              <option value="2">Inactive</option>
            </select>
          </div>
        </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-warning pull-left btn-sm">Add Route</button>
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
          
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif; ?>

<?php if(in_array('updateCategory', $user_permission)): ?>
<!-- edit brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Route</h4>
      </div>

      <form role="form" action="<?php echo base_url('routes/update') ?>" method="post" id="updateForm">

        <div class="modal-body">
          <div id="messages"></div>

          <div class="form-group">
            <label for="edit_routes_name">Route</label>
            <input type="text" class="form-control input-sm" id="edit_routes_name" name="edit_routes_name" placeholder="Enter route name" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_coverage_name">Coverage</label>
            <input type="text" class="form-control input-sm" id="edit_coverage_name" name="edit_coverage_name" placeholder="Enter coverage" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_loads">Loads</label>
            <input type="text" class="form-control input-sm" id="edit_loads" name="edit_loads" placeholder="Enter loads" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_unit">Unit</label>
            <input type="text" class="form-control input-sm" id="edit_unit" name="edit_unit" placeholder="Enter unit" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_returns">Returns</label>
            <input type="text" class="form-control input-sm" id="edit_returns" name="edit_returns" placeholder="Enter return" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_reloads">Reloads</label>
            <input type="text" class="form-control input-sm" id="edit_reloads" name="edit_reloads" placeholder="Enter reload" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_expenses">Expenses</label>
            <input type="text" class="form-control input-sm" id="edit_expenses" name="edit_expenses" placeholder="Enter expenses" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="edit_active">Status</label>
            <select class="form-control input-sm" id="edit_active" name="edit_active">
              <option value="1">Active</option>
              <option value="2">Inactive</option>
            </select>
          </div>
        </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-warning pull-left btn-sm">Save changes</button>
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
          
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif; ?>

<?php if(in_array('deleteRoutes', $user_permission)): ?>
<!-- remove brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Delete Route</h4>
      </div>

      <form role="form" action="<?php echo base_url('routes/remove') ?>" method="post" id="removeForm">
        <div class="modal-body">
          <p>Do you really want to delete?</p>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-warning pull-left btn-sm">Delete</button>
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        
        </div>
      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif; ?>

<script type="text/javascript">
var manageTable;
//Date picker
   $('#datepicker').datepicker({
      autoclose: true
      
   });
   
   $(document).ready(function() {
      $("#routeNav").addClass('active');

      // initialize the datatable 
      manageTable = $('#manageTable').DataTable({
         'ajax': 'fetchRoutesData',
         'order': []
      });

      // submit the create from 
      $("#createForm").unbind('submit').on('submit', function() {
         var form = $(this);

         // remove the text-danger
         $(".text-danger").remove();

         $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize(), // /converting the form data into array and sending it to server
            dataType: 'json',
            success:function(response) {

            manageTable.ajax.reload(null, false); 

            if(response.success === true) {
               $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                  '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
               '</div>');


               // hide the modal
               $("#addModal").modal('hide');

               // reset the form
               $("#createForm")[0].reset();
               $("#createForm .form-group").removeClass('has-error').removeClass('has-success');

            } else {

               if(response.messages instanceof Object) {
                  $.each(response.messages, function(index, value) {
                  var id = $("#"+index);

                  id.closest('.form-group')
                  .removeClass('has-error')
                  .removeClass('has-success')
                  .addClass(value.length > 0 ? 'has-error' : 'has-success');
                  
                  id.after(value);

                  });
               } else {
                  $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                  '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
                  '</div>');
               }
            }
            }
         }); 

         return false;
      });

   });

// edit function
function editFunc(id)
{ 
  $.ajax({
    url: 'fetchRoutesDataById/'+id,
    type: 'post',
    dataType: 'json',
    success:function(response) {

      $("#edit_routes_name").val(response.name);
      $("#edit_coverage_name").val(response.coverage);
      $("#edit_loads").val(response.loads);
      $("#edit_unit").val(response.unit);
      $("#edit_returns").val(response.return);
      $("#edit_reloads").val(response.reload);
      $("#edit_expenses").val(response.expenses);
      $("#edit_active").val(response.active);

      // submit the edit from 
      $("#updateForm").unbind('submit').bind('submit', function() {
        var form = $(this);

        // remove the text-danger
        $(".text-danger").remove();

        $.ajax({
          url: form.attr('action') + '/' + id,
          type: form.attr('method'),
          data: form.serialize(), // /converting the form data into array and sending it to server
          dataType: 'json',
          success:function(response) {

            manageTable.ajax.reload(null, false); 

            if(response.success === true) {
              $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
              '</div>');


              // hide the modal
              $("#editModal").modal('hide');
              // reset the form 
              $("#updateForm .form-group").removeClass('has-error').removeClass('has-success');

            } else {

              if(response.messages instanceof Object) {
                $.each(response.messages, function(index, value) {
                  var id = $("#"+index);

                  id.closest('.form-group')
                  .removeClass('has-error')
                  .removeClass('has-success')
                  .addClass(value.length > 0 ? 'has-error' : 'has-success');
                  
                  id.after(value);

                });
              } else {
                $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                  '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
                '</div>');
              }
            }
          }
        }); 

        return false;
      });

    }
  });
}

   // remove functions 
function removeFunc(id)
{
  if(id) {
    $("#removeForm").on('submit', function() {

      var form = $(this);

      // remove the text-danger
      $(".text-danger").remove();

      $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: { route_id:id }, 
        dataType: 'json',
        success:function(response) {

          manageTable.ajax.reload(null, false); 

          if(response.success === true) {
            $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
              '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
            '</div>');

            // hide the modal
            $("#removeModal").modal('hide');

          } else {

            $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
              '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
            '</div>'); 
          }
        }
      }); 

      return false;
    });
  }
}
 </script>