 
<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>DBEI</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>DBEI</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        
      </a>
     
      <p class="navbar_name"><b>DANICA Basic Essentials, Inc.</b> (for Magnolia Chicken Distributor) </p>
      <!-- <p class="text-muted pull-right clearfix title_name"><b>DANICA Basic Essentials, Inc.</b></p> -->

    </nav>
  </header>
  <!-- Navbar name -->
  <style>
    .navbar_name {
      padding-top: 15px;
      color: white;
      font-weight: 500;
    }
  </style>
  <!-- Left side column. contains the logo and sidebar -->
  